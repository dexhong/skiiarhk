<?php

$server = 'localhost';
$host = 'id8671363_skii';
$pass = '123qweASD';
$db = 'id8671363_skar';

echo '<!DOCTYPE html>
<html>
<head>
<style>
body
{
    font-family: calibri;
}
table
{
  border-collapse: collapse;
  width: 100%;
}

td, th
{
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even)
{
  background-color: #dddddd;
}
</style>
</head>
<body>

<h2>Result Table</h2>';

$conn = new mysqli($server, $host, $pass);
if(mysqli_connect_errno())
{
    //die ("Failed to connect to MySQL: " . mysqli_connect_error());
    echo '-1';
}
else
{
    mysqli_select_db($conn, $db);

    $data_id = mysqli_real_escape_string($conn,$_GET['id']);

    $select_id_sql = "SELECT * FROM data_collection WHERE data_id = " . $data_id . " ORDER BY date DESC;";

    $result = mysqli_query($conn, $select_id_sql);
    if($result->num_rows > 0)
    {
        echo '<table><tr><td>ID</td><td>Data</td><td>date</td></tr>';
        while($row = $result->fetch_assoc())
        {
            echo '<tr>';
            echo '<td>' . $row["id"] . '</td><td>' . urldecode($row["data"]) . '</td><td>' . $row["date"] . '</td>';
            echo '</tr>';
        }
        echo '</table>';
    }
    $conn->close();
}

echo '</body></html>';
?>