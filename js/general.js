// # -- General Use functions -- #

// Get the index cooresponding to the language
// lang is optional
function GetLanguageIndex(lang)
{
  if(lang == null) lang = GetLanguage();
  
  switch(lang)
  {
    case 'zh-cn':
    case 'zh-tw':
    case 'zh-hk':
    case 'zh':
    case 'zh-sg':
      return 0;
    case 'ja':
    case 'jp':
      return 2;
  }
  return 1;
}

// Get the full video path (string) which takes into account for wide aspect ratio
function GetVideo(basePath, video)
{
  return basePath + (isiPad ? wideAspectRatioPrefix : '') + video;
}

function PlayVideoArray(path, vArray, looped)
{
  var body = '';
  for(var i = 0; i < vArray.length; ++i)
  {
    body += '<div id="video' + i.toString() + '" style="position:absolute; width:100%; height:' + (lid == i ? '100%' : '0%') + '; top:0%; left:0%; z-index:' + (lid == i ? 1 : 0).toString() + '; overflow:hidden; pointer-events:none;">'
          + '<video autoplay ' + (looped != null && looped ? 'loop' : '') + ' muted playsinline id="full-video' + i.toString() + '">'
          + '<source id="full-video-src' + i.toString() + '" src="' + cachedVideos[GetVideo(path, vArray[i])] + '" type="video/mp4" />'
          + '</video>'
          + '</div>';
  }
           
  videoContainer.innerHTML = body;
}

function ClearVideo()
{
  videoContainer.innerHTML = '';
}

function toDegree(x)
{
  return x * 180.0 / Math.PI;
}

function FadeOut(cbFunc)
{
  var fader = document.getElementById('fader-screen');
  fader.style.opacity = '0.0';
  
  var faderOpacity = 0.0;
  var intv = setInterval(function()
  {
    faderOpacity += 0.05 * fadingSpeed;
    fader.style.opacity = faderOpacity.toString();
    if(faderOpacity >= 1.0)
    {
      clearInterval(intv);
      
      if(cbFunc != null) cbFunc();
    }
  }, 50);
}

function FadeIn(cbFunc)
{
  var fader = document.getElementById('fader-screen');
  fader.style.opacity = '1.0';
  
  var faderOpacity = 1.0;
  var intv = setInterval(function()
  {
    faderOpacity -= 0.05 * fadingSpeed;
    fader.style.opacity = faderOpacity.toString();
    if(faderOpacity <= 0.0)
    {
      clearInterval(intv);
      
      if(cbFunc != null) cbFunc();
    }
  }, 50);
}

var cachedImages = new Array();
var cachedVideos = new Array();

function PreloadVideos(pathArray, cbFunc)
{
  var overlay = document.getElementById('loading-overlay');
  var loaderText = document.getElementById('progress-count');
  var loaderBar = document.getElementById('progress-bar');
  
  var videoCount = pathArray.length;
  var maxCount = videoCount;
  
  var overallProgress = 0;
  
  loadVideo = function(vidPath, id)
  {
    var prevLoadProgress = 0;
    //console.log('loading video ' + id);
    var xhr = new XMLHttpRequest();
    xhr.open("GET", vidPath, true);
    xhr.responseType = 'arraybuffer';
    xhr.onload = function(e)
    {
      var blob = new Blob([e.target.response],{type: "video/mp4"});
      var v = URL.createObjectURL(blob);
      cachedVideos[vidPath] = v;
      //console.log('loaded video ' + id);
      --videoCount;
      
      if(videoCount <= 0)
        if(cbFunc) cbFunc();
    }
    xhr.onprogress = function(e)
    {
      if(e.lengthComputable)
      {
        var value = Math.floor(100 * e.loaded / e.total);
        var diff = value - prevLoadProgress;
        if(diff > 0)
        {
          overallProgress += diff;
          prevLoadProgress = value;
          
          var percentage = overallProgress * 0.5 / maxCount;
          loaderText.innerHTML = Math.round(percentage) + '%';
          loaderBar.style.width = percentage + '%';
        }
      }
    }
    xhr.send();
  };
  
  for(var i = 0; i < maxCount; ++i)
  {
    loadVideo(pathArray[i],i);
  }
}

function PreloadImages(pathArray, cbFunc)
{
  var overlay = document.getElementById('loading-overlay');
  var loaderText = document.getElementById('progress-count');
  var loaderBar = document.getElementById('progress-bar');
  
  //console.log("loading images");
  var imageCount = pathArray.length;
  var maxCount = imageCount;
  
  for(var i = 0; i < maxCount; ++i)
  {
    cachedImages[i] = new Image();
    cachedImages[i].src = pathArray[i];
    cachedImages[i].onload = function()
    {
      --imageCount;
      if(imageCount <= 0)
      {
        loaderBar.id = 'progress-done';
        //console.log("loaded images : left " + imageCount);
        setTimeout(function()
        {
          overlay.innerHTML = '';
          overlay.style.opacity = '0.0';
          isAssetsLoaded = true;
          if(cbFunc) cbFunc();
        }, 1000);
      }
      else
      {
        var percentage = (cacheVideos ? (50 + (1.0 - (imageCount / maxCount)) * 50)
                                      : ((1.0 - (imageCount / maxCount)) * 100));
        
        //console.log("loaded : images left " + imageCount);
        loaderText.innerHTML = Math.round(percentage) + '%';
        loaderBar.style.width = percentage + '%';
      }
    }
    //console.log(pathArray[i]);
  }
}

function LoadAssets(cbFunc)
{
  var imagePathArray = [];
  
  for(var k = 0; k < assetsToLoad.length; ++k)
  {
    var fullPath = assetPath + imagePath + assetsToLoad[k][0];
    for(var i = assetsToLoad[k][1]; i <= assetsToLoad[k][2]; ++i)
    {
      imagePathArray.push(fullPath + GetNumberString(i,assetsToLoad[k][3]) + '.png');
    }
  }
  
  for(var i = 0; i < languageArray.length; ++i)
  {
    var congratsPath = getCongratsScreen(assetPath + imagePath, i);
    imagePathArray.push(congratsPath);
  }
  
  imagePathArray.push(assetPath + imagePath + 'mini_video_loading.png');
  
  imagePathArray.push(assetPath + imagePath + maskAsset);
  imagePathArray.push(assetPath + imagePath + cantDetectFaceAsset);
  imagePathArray.push(assetPath + imagePath + cantDetectFaceAssetiPhoneX);
  imagePathArray.push(assetPath + imagePath + 'obc.png');
  
  if(!cacheVideos)
  {
    PreloadImages(imagePathArray, cbFunc);
    return;
  }
  
  var videoPathArray = [];
  
  for(var i = 0; i < welcomeVideoPath.length; ++i)
  {
    videoPathArray.push(GetVideo(assetPath + videoPath, welcomeVideoPath[i]));
  }
  
  PreloadVideos(videoPathArray,function()
  {
    PreloadImages(imagePathArray, cbFunc);
  });
}

// # ----------------------------------- #