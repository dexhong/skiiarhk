// #-- Text -------------------------#
var steps =
[
  // This text is replaced with simplified chinese in japan.js
  [
    '第一步：' , '取3-5滴精华于掌心，双手均匀沾湿，\n从鼻子开始，由内向外至两颊处轻拍。' ,
    '第二步：' , '再次取 3-5滴精华于掌心，从眼周向太阳穴方向轻拍。' ,
    '第三步：' , '接着，轻拍前额。' ,
    '第四步：' , '接下来，沿着下巴和下颌轮廓轻拍。' ,
    '第五步：' , '沿着颈部，从上往下轻拍，\n继而再次轻柔拍打整个面部，直至精华完全吸收。' ,
    '' , ''
  ],
  [
    'STEP 1' , 'Fill your palm with 3-5 drops of essence.\nPress palms together to wet both hands.\nStarting at your nose, pat outwards onto your cheeks.' ,
    'STEP 2' , 'Fill palms again with 3-5 drops of essence and\ngently pat over your eye area outwards to your temples.' ,
    'STEP 3' , 'Now, pat across your forehead.' ,
    'STEP 4' , 'Next, pat along your chin and jawline.' ,
    'STEP 5' , 'Pat downwards from the top of your neck.\nTo finish, continue patting your entire face\nuntil the essence is fully absorbed.' ,
    '' , ''
  ],
  [
    'ステップ 1：' , 'まずエッセンスを手のひらにとります。 両方の手のひらを合わせて\n頬を内側から外側に向かって、優しく押さえるようになじませます。' ,
    'ステップ 2：' , 'では次に、もう一度エッセンスを手のひらにとって、\n目の周りをこめかみに向かって丁寧になじませます。' ,
    'ステップ 3：' , '次に額もやさしく押さえるようになじませます。' ,
    'ステップ 4：' , '次はあごとフェイスラインです。こちらもやさしく押さえるように\nなじませましょう。' ,
    'ステップ 5：' , '次は首筋やデコルテを優しく押さえるようになじませましょう。\n 最後にお顔全体をもう一度手のひらで優しくおさえるようにして、\nお肌がしっとりするまでエッセンスをなじませます。' ,
    '' , ''
  ]
];

var bringFaceCloser = 
[
   '要開始體驗，請先將面部對準上方的框架裝置。再嘗試向後或向前移動。',
   'To begin the experience, align your face within the outline above. Try moving backwards or forwards.',
   '枠の中に顔の輪郭を合わせて始めましょう。前後に移動して、枠に顔の輪郭を合わせてせてください。'
];

var titleText =
[
  '欢迎来到护肤精华露AR美肤教程' ,
  'Welcome to the Facial Treatment Essence augmented reality beauty tutorial',
  'フェイシャル トリートメント エッセンスの AR チュートリアルにようこそ'
];
// #---------------------------------#