// #-- Settings ---------------------#
var debugMode = false;
var cacheVideos = true;
var enablePlayer = false;
var repeatAnimation = false;
var autoReloadMode = true;
var autoReloadDelay = 10.0;
var endScreenAutoReloadDelay = 20.0;

var landmarkHistoryCount = 50;

var animationSpeed = 1.0; // N x speed
var curtainTransitionSpeed = 0.2;
var fadingSpeed = 2.0;
var countdownSpeed = 1.0;

// IMPORTANT NOTE: These 2 language arrays have to be the same size
var languageArray = [ 'zh', 'en-US', 'jp']; // 'jp'      // Array of different languages
var languageButtonArray = [ 'ch_', 'en_', 'jp_'];//'jp_'  // Asset names for language buttons
var languageFontArray = ['skiiReg', 'skiicap'];
var languageBoldFontArray = ['skii', 'skiicapBold'];

var videoObj = document.getElementById('full-video');
var videoSrcObj = document.getElementById('full-video-src');
var videoContainer = document.getElementById('video-container');

var layers = 
[
  document.getElementById('background'),
  document.getElementById('ground'),
  document.getElementById('foreground'),
  document.getElementById('debug-layer')
];

var videoBackground = document.getElementById('video-background');

// #-- Boolean states, offsets and setting values ---#
var aspectRatio = window.innerHeight / window.innerWidth;
var isPaused = false;
var isTutorialMode = false;
var isFaceDetected = false;
var isDoneFadingOut = false;
var isPlayerOpenned = false;
var isDetectPaused = false;
var isMiniVideoPlaying = false;
var isAssetsLoaded = false;
var isInTutorial = false;
var currentStep = 0;
var currentAnimation = null;
var currentFullScreenAnimation = null;
var autoReloadDelayCounter = 0;
var progressValue = 0;
var displayProgressValue = 10;
var cantDetectFaceOpacity = 0;
var countdownTimer = 0;

var cameraXOffset = 0.5;
var cameraXTargetOffset = 0.5;
var cameraXPreviousOffset = 0.5;

const minValue = 0.28125;
const maxValue = 0.70315;
const diffValue = maxValue - minValue;

var landmarkPositionHistory = [];
var nextLandmarkHistoryID = 0;

var imgOffsetX = 1.0;
var imgOffsetY = 1.0;

var standalone = window.navigator.standalone,
userAgent = window.navigator.userAgent.toLowerCase(),
//safari = /safari/.test( userAgent ),
isiPad = /ipad/.test( userAgent );


console.log(isiPad ? 'IS IPAD' : 'IS NOT IPAD');

// #-- Animation Settings -----------#

var animImgScaleX = 2000;
var animImgScaleY = 2000;
// IPAD OFFSETS
var animImgOffsetX = [0.45,0.45,0.45,0.45,0.47];
var animImgOffsetY = [0.35,0.37,0.34,0.35,0.34];
// PHONE OFFSETS
var animImgOffsetX_phone = 0.365;
var animImgOffsetY_phone = [0.29,0.33,0.35,0.28,0.27];

var animImgFrames = [100,200,300,400,504];

// #---------------------------------#


// #-- Assets -----------------------#
var wideAspectRatioPrefix = 'ipad_';

var assetPath = './assets/';
var imagePath = 'images/'; // ./assets/images/
var videoPath = 'videos/'; // ./assets/videos/

var maskAsset = 'resizedMask.png';
var cantDetectFaceAsset = 'facedetectSquare_ahma.png';
var cantDetectFaceAssetiPhoneX = 'facedetect_iphonex.png';
var cantDetectFaceTextAsset = ['cantDetectFace_jp.png','cantDetectFace_cn.png','cantDetectFace_en.png',];

var detectionMaskSize = 2;

var welcomeVideoPath =
[
  'welcome_cn.mp4',
  'welcome_en.mp4'

];

var endingPicPath = 
[
  'Congrats_SC.jpg',
  'Congrats_EN.jpg',
  'Congrats_JP.jpg'
];

var buttonText = 
[
  'cnbtn.png',
  'enbtn.png',
  'jpnbtn.png'
];

var selectedButton = 
[
  'cnSelected.png',
  'enSelected.png',
  'jpnSelected.png'
];

var tutorialAnimationPath = 'HandsAnimation/handsSequence';
var curtainEffectPath = 'Curtain_wipe_sequence/Curtain wipe_';
var sparkleEffectPath = 'Sparkle_sequence/celebratory_sparkles';

// Path , start frame, end frame, size of digits
var assetsToLoad =
[
  [tutorialAnimationPath,0,504,4],
  [curtainEffectPath,1,51,2],
  [sparkleEffectPath,0,58,2]
];
// #---------------------------------#