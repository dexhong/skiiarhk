// #---- Functionality and operations for Welcome Screen --#
steps[0] =
[
  '第一步：' , '取3-5滴神仙水於掌心，雙手均勻沾濕，從鼻子開始，\n由內向外至面頰輕拍。' ,
  '第二步：' , '再次取 3-5滴神仙水於掌心，\n用指腹從眼周向太陽穴方向輕拍。' ,
  '第三步：' , '接著，用掌心輕拍前額。' ,
  '第四步：' , '接下來，沿著下巴及腮骨(輪廓位置)輕拍。' ,
  '第五步：' , '沿著頸部，從上往下輕拍，繼而再次輕柔拍打整個面部，\n直至神仙水完全吸收。' ,
  '' , ''
];

bringFaceCloser[0] = '要開始體驗，請將您的面部對准輪廓的框線。嘗試前後移動調整位置。';


// Returns the current browser language if uri param does not specify
function GetLanguage()
{
	var url = new URL(window.location);
	var lang = url.searchParams.get("lang");
	if(lang == null)
	{
		//lang = navigator.languages ? navigator.languages[0] : window.navigator.userLanguage;
		if(lang == null)
			lang = 'zh';
	}
  if(debugMode) console.log('Chosen Language: ' + lang);
	
	return lang;
}

function ChangeLanguage(lang)
{
  if(!isAssetsLoaded) return;
  var lastLid = lid;
  lid = GetLanguageIndex(lang);
  currentLanguage = lang;
  
  var currTime = document.getElementById('full-video' + lastLid.toString()).currentTime;
  document.getElementById('full-video' + lid.toString()).currentTime = currTime;
  
  for(var i = 0; i < welcomeVideoPath.length; ++i)
  {
    var vid = document.getElementById('video' + i.toString());
    vid.style.zIndex = (lid == i ? 1 : 0);
    vid.style.height = (lid == i ? '100%' : '0%');
    
    var langImage = document.getElementById('img-lang' + i.toString());
    if(langImage)
      langImage.src = assetPath + imagePath + languageButtonArray[i] + (lid == i ? 'select' : '') + '.png';
  }
}

function gotoWelcomeScreen()
{
  if(!isTutorialMode) return;
  isTutorialMode = false;
  isInTutorial = false;
  FadeOut(function()
  {
    initWelcomeScreen();
    FadeIn();
  });
}

function initWelcomeScreen()
{
  isInTutorial = false;
  var whiteScreen = '<div style="position:absolute; background-color: #ffffff; top:0;left:0;width:100%;height:100%; z-index:0;"></div>';
  
  videoBackground.innerHTML = whiteScreen;
  
  PlayVideoArray(assetPath + videoPath, welcomeVideoPath, true);
  
  var invButton = '<div id="hidden-button" style="bottom: 5%;width: 100%;height: 3.5%;" onclick="gotoTutorialScreen()"></div>';
  
  var rightOffset = 7; // old value is 1%
  var spaceBetween = 7; // 9%
  
  var langButtons = '';
  
  addLanguageButton = function(i)
  {
      var rightOffset = 2; // old value is 1%
      var spaceBetween = 10; // 9%
      langButtons += '<a href="#" onclick="ChangeLanguage(\'' + languageArray[i] + '\'); return false;">'
                  + '<img class="img-lang" id="img-lang' + i.toString() + '" src="' + assetPath + imagePath + languageButtonArray[i] + (lid == i ? 'select' : '') + '.png" style="top:86%;right:' + (rightOffset + (languageArray.length - i - 2) * spaceBetween).toString() + '%;height:5%;width:auto;"/>'
                  + '</a>';        

  };
  
  addLanguageButton(0);
  addLanguageButton(1);
  
  var body = invButton + langButtons;
  layers[2].innerHTML = body;
}

// # -------------------------------------- #