// -- Tutorial Screen -----------------------#
var updateHandler = null;       // function pointer for detection mode
var detectTimeThen = 0;         // time then for calculating delta time

// This mode detects the face and countdown to actual tutorial mode
function FaceDetectionMode(face, brfv4)
{
  cameraXPreviousOffset = cameraXOffset;
  cameraXOffset += (cameraXTargetOffset - cameraXOffset) * 0.1;

  if(cameraXPreviousOffset != cameraXOffset)
    onResize(cameraXOffset);

  // Get delta time
  var detectTimeNow = Date.now();
  var dt = (detectTimeNow - detectTimeThen) * 0.001;
  detectTimeThen = detectTimeNow;
  
  var mask = document.getElementById("mask");
  var cdContainer = document.getElementById('ui-countdown');
  
  // If there is no face detected
  if(!isFaceDetected || face.state != brfv4.BRFState.FACE_TRACKING)
  {
    // Reset the countdown and the values of opacity for mask and countdown timer
    if(countdownTimer < 3)
      cantDetectFaceOpacity = 0.9;
    countdownTimer = 3;
    cdContainer.style.opacity = '0.0';
    mask.style.opacity = '0.0';
    isDetectPaused = true;
    // Show cantDetectFaceAsset
    uiFaceCantDetect.style.opacity = cantDetectFaceOpacity.toString();
    cantDetectFaceOpacity = Math.min(1.0,cantDetectFaceOpacity + 0.1);

    // If auto reload is enabled
    if(autoReloadMode)
    {
      // wait till delay is reached
      autoReloadDelayCounter += 0.02;
      if(autoReloadDelayCounter > autoReloadDelay)
      {
        // Destroy and reset everything to default values
        updateHandler = null;
        layers[0].innerHTML = '';
        layers[1].innerHTML = '';
        layers[2].innerHTML = '';
        
        isFaceDetected = false;
        isDoneFadingOut = false;
        firstDetection = false;
        autoReloadDelayCounter = 0;
        progressValue = 0;
        displayProgressValue = 10;
        if(currentAnimation != null)
          clearInterval(currentAnimation);
        if(currentFullScreenAnimation != null)
          clearInterval(currentFullScreenAnimation);
        // Go back to the welcome screen
        gotoWelcomeScreen();
      }
    }
    if(face.state != brfv4.BRFState.FACE_TRACKING_START)
      return;
  }
  // When a face is detected, start counting down
  isDetectPaused = false;
  autoReloadDelayCounter = 0;
  uiFaceCantDetect.style.opacity = cantDetectFaceOpacity.toString();
  cantDetectFaceOpacity = Math.max(0.0,cantDetectFaceOpacity - 0.1);
  
  // Get the x and y position of the face origin ==========================
  var scaleX = face.scale / 480 * (1 - toDegree(Math.abs(face.rotationY)) * divAngleConst) * detectionMaskSize;
  var scaleY = face.scale / 480 * (1 - toDegree(Math.abs(face.rotationX)) * divAngleConst) * detectionMaskSize;
  
  var maskimg = document.getElementById("maskimg");
  var x = face.points[30].x - maskimg.naturalWidth * scaleX *  (imgOffsetX-0.2);//(imgOffsetX+0.6);
  var y = face.points[30].y - maskimg.naturalHeight * scaleY * (imgOffsetY-0.3);//(imgOffsetY+0.3);
  
  y = y * scaleCoeff + webcamOffsetY - toDegree(face.rotationZ) * divAngleConst * scaleCoeff * maskimg.naturalHeight * scaleY * imgOffsetY * detectionMaskSize;
  x = x * scaleCoeff + webcamOffsetX + toDegree(face.rotationZ) * divAngleConst * scaleCoeff * maskimg.naturalWidth * scaleX * imgOffsetX * detectionMaskSize;
  // ======================================================================
  var imageData = document.getElementById("_imageData");
  var xPercentage = face.points[30].x / imageData.width;
  cameraXTargetOffset = Math.max(minValue,Math.min(maxValue,(xPercentage - minValue) / diffValue));

  mask.style.transform = "matrix(" + scaleX + ",0.0,0.0," + scaleY + ","
                       + x + "," + y + ") rotate(" + face.rotationZ + "rad)";
  
  if(isFaceDetected && !isDoneFadingOut)
  {
    uiFaceCantDetect.style.opacity = cantDetectFaceOpacity.toString();
    cantDetectFaceOpacity = Math.max(0.0,cantDetectFaceOpacity - 0.1);
    if(cantDetectFaceOpacity <= 0.0)
    {
      mask.style.opacity = 0;
      cdContainer.style.opacity = '1.0';
      
      countdownTimer -= dt * countdownSpeed;
      if(countdownTimer <= 0)
      {
        cdContainer.innerHTML = '';
        isDoneFadingOut = true;
        StartTutorial();
        updateHandler = TutorialMode;
      }
      else
        cdContainer.innerHTML = Math.ceil(countdownTimer);
    }
  }
}

// Initialize tutorial screen
function initTutorialScreen()
{
  detectTimeThen = Date.now();
  updateHandler = null;
  setTimeout(function()
  {
    updateHandler = FaceDetectionMode;
  },1000);
  var animImg = '<div id="mask" style="position:absolute; top: 0px; left: 0px; opacity: 0;">'
              + '<img id="maskimg" src="' + assetPath + imagePath + maskAsset + '" style="position:absolute;" />';
              + '</div>';
  
  layers[0].innerHTML = animImg;
  var fontsize = (isiPad) ? '3.5' : '3';
  var faceDetectImg = (isiPad) ? 'width:100%; height:100%;"' : 'position:fixed; top:50%; left:50%; transform: translate(-50%, -50%); height:100%;"';
  var textDivs = '<div id="ui-bg">'
               + '<img id="ui-bg-img" src="' + assetPath + imagePath + 'obc.png" width="100%" height="100%" style="height:100%;" />'
               + '</div>'
               + '<div id="ui-bottombox">'
               + '<table><tr><td>'
               + '<div id="ui-title" style="font-family:' + languageFontArray[lid] + '">'
               + '</div></td></tr><tr><td>'
               + '<div id="ui-body" style="font-family:' + languageFontArray[lid] + '">'
               + '</div></td></tr></table>'
               + '</div>'
               + '<div id="ui-countdown"></div>'
               + '<div id="ui-player">'
               + '<span id="dot0" class="dot" style="left:calc(10% - 5px); background-color:#ffffff;"></span>'
               + '<span id="dot1" class="dot" style="left:calc(26% - 5px);"></span>'
               + '<span id="dot2" class="dot" style="left:calc(42% - 5px);"></span>'
               + '<span id="dot3" class="dot" style="left:calc(58% - 5px);"></span>'
               + '<span id="dot4" class="dot" style="left:calc(74% - 5px);"></span>'
               + '<span id="dot5" class="dot" style="left:calc(90% - 5px);"></span>'
               + '<span id="line"></span>'
               
               + '<span id="separator"></span>'
               + '<a href="#" onclick="OnPlayerInteract(1); return false;" ><img src="./assets/images/player/icon-exit.png" id="ico-x" /></a>'
               + '<a href="#" onclick="OnPlayerInteract(2); return false;" ><img src="./assets/images/player/icon-left.png" id="ico-left" /></a>'
               + '<a href="#" onclick="OnPlayerInteract(3); return false;" ><img src="./assets/images/player/icon-right.png" id="ico-right" /></a>'
               + '<a href="#" onclick="OnPlayerInteract(4); return false;" ><img src="./assets/images/player/icon-pause.png" id="ico-pause" /></a>'
               + '<a href="#" onclick="OnPlayerInteract(5); return false;" ><img src="./assets/images/player/icon-play.png" id="ico-play" /></a>'
               + '</div>'
               + '<a href="#" onclick="OnPlayerInteract(0); return false;" ><div id="player-controls"></div></a>'
               
               + '<div id="curtain-container">'
               + '<img id="curtain" style="background: transparent; width:100%; height:100%; opacity:0;" src=""></img>'
               + '</div>'
               + '<div id="ui-facedetect" style="display:block;">'
               + '<img style="background: transparent;' + faceDetectImg +  'src="' + assetPath + imagePath + (aspectRatio > 2.0 ? cantDetectFaceAssetiPhoneX : cantDetectFaceAsset) + '"></img>'
               + '<div id="ui-text" style="position:absolute;top:80%;left:10%;width:80%;height:auto;text-align:center;font-size:'+fontsize+'vh;color:white;font-family:' + languageFontArray[lid] + ';">' + bringFaceCloser[lid] + '</div>'
               //+ '<div id="uiX" style="position:absolute;top:80%;left:10%;width:80%;height:auto;>' + bringFaceCloser[0] + '</div>'
               //+ '<img style="position:absolute; background: transparent; top:80%; left:10%; width:80%; height:auto;" src="' + assetPath + imagePath + cantDetectFaceTextAsset[lid] + '"></img>'
               + '</div>';
               
  layers[2].innerHTML = textDivs;
  
  uiFaceCantDetect = document.getElementById('ui-facedetect');
  document.getElementById('ui-player').style.display = 'none';
  cantDetectFaceOpacity = 1.0;
  
  document.getElementById('ui-title').style.fontSize = isiPad ? '400%' : '200%';
  document.getElementById('ui-body').style.fontSize = isiPad ? '2.5vh' : '2.25vh';
  for(var i = 0; i <= 5; ++i)
  {
    var dot = document.getElementById('dot' + i.toString());
    dot.style.width = isiPad ? '10px' : '4px';
    dot.style.height = isiPad ? '10px' : '4px';
    dot.style.border = (isiPad ? '0.05' : '0.02') + 'em solid white';
  }
  var progressLine = document.getElementById('line');
  progressLine.style.height = isiPad ? '6px' : '3px';
  progressLine.style.bottom = 'calc(6% + ' + (isiPad ? '6' : '2') + 'px)';
  
  if(debugMode)
  {
    var debugStuff = '<div id="debug-container" style="position:absolute; top: 0px; left: 0px; opacity: 1; font-size:50px;">'
              + "Window Inner Height : " + window.innerHeight  + '<br/>'
              + "Window Inner Width :" + window.innerWidth + '<br/>'
              + "Window Aspect Ratio:" + aspectRatio + '<br/>'
              + '</div>';
              
    layers[3].innerHTML = debugStuff;
  }
  

  
  var screenScaleY = layers[0].clientHeight / 1296.0;
  
  var mask = document.getElementById("maskimg");
  mask.style.width = parseInt(/*675 832.5*/ 850 * screenScaleY) + 'px';
  mask.style.height = parseInt(/*1282.5 */ 850 * screenScaleY) + 'px';
  
  //isMiniVideoPlaying = true;
  isPaused = false;
  isDetectPaused = false;
  autoReloadDelayCounter = 0;
  countdownTimer = 3;
  imgOffsetX = 1.0;
  imgOffsetY = isiPad ? 1.0 : 1.25;
  currentStep = 0;
  if(currentAnimation != null)
    clearInterval(currentAnimation);
  if(currentFullScreenAnimation != null)
    clearInterval(currentFullScreenAnimation);
}
// ------------------------------------------#
