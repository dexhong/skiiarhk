var currentLanguage = GetLanguage();
var lid = GetLanguageIndex(currentLanguage);

// Main Function (called once)
(function()
{
  addBRFScript();
  initBRF();
  
  data_get_id();
    
  window.onload = function()
  {
    LoadAssets(function()
    {
      initWelcomeScreen();
      data_collect();
      setTimeout(FadeIn, 1000);
    });
  }
  document.addEventListener('touchmove',function(e){e.preventDefault();}, false);
})();