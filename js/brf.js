// Even for a minimal example there are several functions that are commonly used by all minimal examples, eg. adding
// the correct script (wasm or asm.js), starting the webcam etc.

// Once we know whether wasm is supported we add the correct library script and initialize the example.

var isBRFInitialized = false;

var scaleCoeff = 0;
var webcamOffsetX = 0;
var webcamOffsetY = 0;

var _isWebAssemblySupported = (function()
{
  function testSafariWebAssemblyBug()
  {

    var bin   = new Uint8Array([0,97,115,109,1,0,0,0,1,6,1,96,1,127,1,127,3,2,1,0,5,3,1,0,1,7,8,1,4,116,101,115,116,0,0,10,16,1,14,0,32,0,65,1,54,2,0,32,0,40,2,0,11]);
    var mod   = new WebAssembly.Module(bin);
    var inst  = new WebAssembly.Instance(mod, {});

    return (inst.exports.test(4) !== 0);
  }

  var isWebAssemblySupported = (typeof WebAssembly === 'object');

  if(isWebAssemblySupported && !testSafariWebAssemblyBug())
  {
    isWebAssemblySupported = false;
  }

  return isWebAssemblySupported;
})();

function readWASMBinary(url, onload, onerror, onprogress)
{
  var xhr = new XMLHttpRequest();
  
  xhr.open("GET", url, true);
  xhr.responseType = "arraybuffer";
  xhr.onload = function xhr_onload() {
    if (xhr.status === 200 || xhr.status === 0 && xhr.response)
    {
      onload(xhr.response);
      return;
    }
    onerror();
  };
  xhr.onerror = onerror;
  xhr.onprogress = onprogress;
  xhr.send(null);
}

function addBRFScript()
{
  var script = document.createElement("script");
  
  script.setAttribute("type", "text/javascript");
  script.setAttribute("async", true);
  script.setAttribute("src", brfv4BaseURL + brfv4SDKName + ".js");
  
  document.getElementsByTagName("head")[0].appendChild(script);
}

var brfv4Example = { stats: {} };
var brfv4BaseURL = _isWebAssemblySupported ? "js/libs/brf_wasm/" : "js/libs/brf_asmjs/";
var brfv4SDKName = "BRFv4_JS_BA180918_v4.1.0b_commercial"; // the currently available library
var brfv4WASMBuffer = null;

var handleTrackingResults = function(brfv4, faces, imageDataCtx)
{
  // Overwrite this function in your minimal example HTML file.
  
  for(var i = 0; i < faces.length; i++)
  {
    var face = faces[i];
    
    if(face.state === brfv4.BRFState.FACE_TRACKING_START ||
      face.state === brfv4.BRFState.FACE_TRACKING)
    {
      imageDataCtx.strokeStyle="#00a0ff";

      for(var k = 0; k < face.vertices.length; k += 2)
      {
        imageDataCtx.beginPath();
        imageDataCtx.arc(face.vertices[k], face.vertices[k + 1], 2, 0, 2 * Math.PI);
        imageDataCtx.stroke();
      }
    }
  }
};

function initBRF()
{

  // This function is called after the BRFv4 script was added.

  // BRFv4 needs the correct input image data size for initialization.
  // That's why we need to start the camera stream first and get the correct
  // video stream dimension. (startCamera, onStreamFetched, onStreamDimensionsAvailable)

  // Once the dimension of the video stream is known we need to wait for
  // BRFv4 to be ready to be initialized (waitForSDK, initSDK)

  // Once BRFv4 was initialized, we can track faces (trackFaces)

  var webcam        = document.getElementById("_webcam");     // our webcam video
  var imageData     = document.getElementById("_imageData");  // image data for BRFv4
  var imageDataCtx  = null;                                   // only fetch the context once

  var brfv4         = null; // the library namespace
  var brfManager    = null; // the API
  var resolution    = null; // the video stream resolution (usually 640x480)
  var timeoutId     = -1;

  // iOS has this weird behavior that it freezes the camera stream, if the CPU get's
  // stressed too much, but it doesn't unfreeze the stream upon CPU relaxation.
  // A workaround is to get the video stream dimension and then turn the stream off
  // until BRFv4 was successfully initialized (takes about 3 seconds of heavy CPU work).

  var isIOS = (/iPad|iPhone|iPod/.test(window.navigator.userAgent) && !window.MSStream);

  startCamera();

  function startCamera()
  {
    console.log("startCamera");
    
    // Start video playback once the camera was fetched to get the actual stream dimension.
    function onStreamFetched (mediaStream)
    {
      console.log("onStreamFetched");
      
      webcam.srcObject = mediaStream;
      webcam.play();
      
      // Check whether we know the stream dimension yet, if so, start BRFv4.
      function onStreamDimensionsAvailable ()
      {
        console.log("onStreamDimensionsAvailable: " + (webcam.videoWidth !== 0));

        if(webcam.videoWidth === 0)
        {
          setTimeout(onStreamDimensionsAvailable, 100);
        }
        else
        {
          imageData.width = webcam.videoWidth;
          imageData.height = webcam.videoHeight;
          
          imageDataCtx = imageData.getContext("2d");
          
          window.addEventListener("resize", onResize);
          onResize();
          
          isBRFInitialized = true;
          // on iOS we want to close the video stream first and
          // wait for the heavy BRFv4 initialization to finish.
          // Once that is done, we start the stream again.
          
          // as discussed above, close the stream on iOS and wait for BRFv4 to be initialized.
          
          if(isIOS)
          {
            webcam.pause();
            webcam.srcObject.getTracks().forEach(function(track)
            {
              track.stop();
            });
          }
          
          waitForSDK();
        }
      }

      // imageDataCtx is not null if we restart the camera stream on iOS.

      if(imageDataCtx === null)
      {
        onStreamDimensionsAvailable();
      }
      else
      {
        trackFaces();
      }
    }
    
    window.navigator.mediaDevices.getUserMedia({ video: { width: 640, height: 480, frameRate: 30 } })
      .then(onStreamFetched).catch(function () { alert("No camera available."); });
  }

  function waitForSDK()
  {
    if(brfv4 === null && window.hasOwnProperty("initializeBRF"))
    {
      brfv4 = {
        locateFile: function(fileName) { return brfv4BaseURL + fileName; },
        wasmBinary: brfv4WASMBuffer
      };
      
      initializeBRF(brfv4);
    }

    if(brfv4 && brfv4.sdkReady)
    {
      initSDK();
    }
    else
    {
      setTimeout(waitForSDK, 250); // wait a bit...
    }
  }

  function initSDK()
  {
    resolution = new brfv4.Rectangle(0, 0, imageData.width, imageData.height);
    brfManager = new brfv4.BRFManager();
    brfManager.init(resolution, resolution, "com.tastenkunst.brfv4.js.examples.minimal.webcam");
    
    //var faceDetectionRoi = new brfv4.Rectangle();
    //faceDetectionRoi.setTo(
    //    resolution.width * 0.25, resolution.height * 0.10,
    //    resolution.width * 0.50, resolution.height * 0.80
    //);
    //brfManager.setFaceDetectionRoi(faceDetectionRoi);
    //var maxFaceSize = Math.max(faceDetectionRoi.height,faceDetectionRoi.width);
    //
    //brfManager.setFaceDetectionParams(maxFaceSize * 0.30, maxFaceSize * 0.90, 12, 8);
    
    if(isIOS)
    {
      // Start the camera stream again on iOS.
      setTimeout(function ()
      {
        console.log('delayed camera restart for iOS');
        startCamera();
      }, 100);
    }
    else
    {
      trackFaces();
    }
  }

  function trackFaces()
  {
    if(brfv4Example.stats.start) brfv4Example.stats.start();
    
    var timeStart = window.performance.now();
    
    imageDataCtx.setTransform(-1.0, 0, 0, 1, resolution.width, 0); // A virtual mirror should be... mirrored
    imageDataCtx.drawImage(webcam, 0, 0, resolution.width, resolution.height);
    imageDataCtx.setTransform( 1.0, 0, 0, 1, 0, 0); // unmirrored for drawing the results
    
    brfManager.update(imageDataCtx.getImageData(0, 0, resolution.width, resolution.height).data);
    
    handleTrackingResults(brfv4, brfManager.getFaces(), imageDataCtx);
    
    if(brfv4Example.stats.end) brfv4Example.stats.end();
    
    if(timeoutId >= 0)
    {
      clearTimeout(timeoutId);
    }
    
    var elapstedMs = window.performance.now() - timeStart;
    timeoutId = setTimeout(function() { trackFaces(); }, (1000 / 30) - elapstedMs);
  }
}

function onResize(xScale, yScale)
{
  var imageData = document.getElementById("_imageData");
  var ww = window.innerWidth;
  var wh = window.innerHeight;
  var s = wh / imageData.height;
  if (imageData.width * s < ww)
  {
    s = ww / imageData.width;
  }
  var iw = imageData.width * s;
  var ih = imageData.height * s;
  var ix = (ww - iw) * (xScale != null ? xScale : 0.5);
  var iy = (wh - ih) * (yScale != null ? yScale : 0.5);
  imageData.style.transformOrigin = "0% 0%";
  
  webcamOffsetX = ix;
  webcamOffsetY = iy;
  
  scaleCoeff = s;
  //console.log(s);
  imageData.style.transform = "matrix(" + s + ", 0, 0, " + s + ", " + ix + ", " + iy + ")";
};