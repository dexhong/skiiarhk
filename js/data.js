var data_id = -1;
var data_use_count = [0,0,0]; // CN , EN , JP
var data_complete_count = [0,0,0] // CN , EN , JP

function data_add_use_count(value)
{
    data_use_count[lid] += (value != null ? value : 1);
}

function data_add_complete_count(value)
{
    data_complete_count[lid] += (value != null ? value : 1);
}

function data_get_id()
{
    /*var req = new XMLHttpRequest();
    req.open('GET', 'data/get_id.php', false);
    req.send(null);
    data_id = parseInt(req.responseText);
    console.log('data_id: ' + data_id);

    document.getElementById('data-layer').innerHTML = data_id;*/
    return -1;
    jQuery.ajax({
        type: "GET",
        url: 'data/get_id.php',
        dataType: 'text',
        success: function(obj, status)
        {
            if(status === 'success')
            {
                data_id = parseInt(obj);
                if(isNaN(data_id))
                data_id = -1;
                console.log('data_id: ' + data_id);
                document.getElementById('data-layer').innerHTML = data_id;
            }
        }
    });
}

function data_collect()
{
    if(data_id == -1) return;

    var data_string = 'aspectRatio: ' + aspectRatio + '<br/>'
                    + 'isPaused: ' + isPaused + '<br/>'
                    + 'isTutorialMode: ' + isTutorialMode + '<br/>'
                    + 'isFaceDetected: ' + isFaceDetected + '<br/>'
                    + 'isDoneFadingOut: ' + isDoneFadingOut + '<br/>'
                    + 'isPlayerOpenned: ' + isPlayerOpenned + '<br/>'
                    + 'isDetectPaused: ' + isDetectPaused + '<br/>'
                    + 'isAssetsLoaded: ' + isAssetsLoaded + '<br/>'
                    + 'isInTutorial: ' + isInTutorial + '<br/>'
                    + 'currentStep: ' + currentStep + '<br/>'
                    + 'currentAnimation: ' + currentAnimation + '<br/>'
                    + 'currentFullScreenAnimation: ' + currentFullScreenAnimation + '<br/>'
                    + 'autoReloadDelayCounter: ' + autoReloadDelayCounter + '<br/>'
                    + 'progressValue: ' + progressValue + '<br/>'
                    + 'displayProgressValue: ' + displayProgressValue + '<br/>'
                    + 'cantDetectFaceOpacity: ' + cantDetectFaceOpacity + '<br/>'
                    + 'countdownTimer: ' + countdownTimer + '<br/>'
                    + 'data_use_count[CH]: ' + data_use_count[0] + '<br/>'
                    + 'data_use_count[EN]: ' + data_use_count[1] + '<br/>'
                    + 'data_use_count[JP]: ' + data_use_count[2] + '<br/>'
                    + 'data_complete_count[CH]: ' + data_complete_count[0] + '<br/>'
                    + 'data_complete_count[EN]: ' + data_complete_count[1] + '<br/>'
                    + 'data_complete_count[JP]: ' + data_complete_count[2] + '<br/>';

    /*var req = new XMLHttpRequest();
    console.log('?id=' + data_id + '&dat=' + encodeURIComponent(data_string));
    req.open('GET', 'data/collect.php?id=' + data_id + '&dat=' + encodeURIComponent(data_string), true);
    req.send(null);*/

    jQuery.ajax({
        type: "GET",
        url: 'data/collect.php',
        dataType: 'text',
        data: { id: data_id, dat:encodeURIComponent(data_string) },
        success: function(obj, status)
        {
            if(status === 'success')
            {
                console.log(obj);
            }
        }
    });

    setTimeout(data_collect,1000 * 60 * 30); // 30 mins
    //setTimeout(data_collect,1000 * 30); // 30 secs
}