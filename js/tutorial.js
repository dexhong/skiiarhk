// -- Tutorial Screen -----------------------#
var updateHandler = null;

function GetNumberString(x, size)
{
  var str = String(x);
  while(str.length < (size || 2))
    str = '0' + str;
  return str;
}

var videoBoxCounter = 0;
function SetMiniVideo(path, cbFunc)
{
  var miniVideo = '<div id="video-box">'
                //+ '<video src="' + path + '" poster="' + assetPath + imagePath + 'mini_video_loading.png" loop autoplay muted playsinline controls="true" id="videobox' + videoBoxCounter + '">'
                //+ '<source src="' + path + '" type="video/mp4" />'
                //+ '<source src="' + assetPath + imagePath + 'mini_video_loading.png" type="image/png" />'
                //+ '</video>'
                + '<img id="mini-video" style="top:2.5%; left:5%; margin-top:50px; margin-left:25px; width:50%;" src="' + path + '" />'
                + '</div>';
  layers[1].innerHTML = miniVideo;
  
  setTimeout(function()
  {
    document.getElementById('mini-video').src = '';
    if(cbFunc != null)
      cbFunc();
  },4000);
}

function PlayFullScreenAnimation(imgObj, path, startIndex, endIndex, size, fps, cbFunc)
{
  var currIndex = startIndex;
  
  var currentTime = 0;
  var spf = 1.0 / (fps * curtainTransitionSpeed * animationSpeed);
  var timeThen = Date.now();
  
  var intv = setInterval(function()
  {
    timeNow = Date.now();
    var dt = (timeNow - timeThen) * 0.001;
    timeThen = timeNow;
    
    currentTime += dt;
    var multiple = Math.floor(currentTime / spf);
    currentTime -= spf * multiple;
    currIndex = Math.min(endIndex, currIndex + multiple);
    
    imgObj.src = path + GetNumberString(currIndex, size) + '.png';
    imgObj.style.opacity = '1.0';
    if(currIndex == endIndex)
    {
      clearInterval(intv);
      currentFullScreenAnimation = null;
      if(cbFunc != null)
        cbFunc();
    }
  }, 0);//parseInt(1000.0 / (fps * animationSpeed)));
  currentFullScreenAnimation = intv;
  
  imgObj.src = path + GetNumberString(currIndex, size) + '.png';
}

function PlayAnimation(path, startIndex, endIndex, size, fps, offsetX, offsetY, cbFunc)
{
  var currIndex = startIndex;
  var imgObj = document.getElementById('maskimg');
  var mask = document.getElementById("mask");
  var progressBar = document.getElementById('line');
  
  var screenScaleY = layers[0].clientHeight / 1296.0;
  
  imgObj.style.width = parseInt(animImgScaleX * screenScaleY) + 'px';
  imgObj.style.height = parseInt(animImgScaleY * screenScaleY) + 'px';
    
  imgOffsetX = offsetX;
  imgOffsetY = offsetY;
  
  var currentTime = 0;
  var spf = 1.0 / (fps * animationSpeed);
  var timeThen = Date.now();

  var playedOnce = !repeatAnimation;
  
  var intv = setInterval(function()
  {
    timeNow = Date.now();
    var dt = (timeNow - timeThen) * 0.001;
    timeThen = timeNow;
    
    if(isPaused || isDetectPaused) return;
    mask.style.opacity = 0.9;
    
    //++currIndex;
    currentTime += dt;
    var multiple = Math.floor(currentTime / spf);
    currentTime -= spf * multiple;
    currIndex = Math.min(endIndex, currIndex + multiple);
    /*while(currentTime > spf)
    {
      currIndex = Math.min(endIndex,currIndex + 1);
      currentTime -= spf;
    }*/
    
    imgObj.src = path + GetNumberString(currIndex, size) + '.png';
    
    if(repeatAnimation)
    {
      if(!playedOnce)
        progressValue = (currIndex - startIndex) / (2 * (endIndex - startIndex));
      else
        progressValue = ((endIndex - startIndex) + (currIndex - startIndex)) / (2 * (endIndex - startIndex));
    }
    else
      progressValue = (currIndex - startIndex) / (endIndex - startIndex);
    //console.log(displayProgressValue + (progressValue * 16));
    progressBar.style.width = parseFloat(displayProgressValue + (progressValue * 16)) + '%';
    
    if(currIndex == endIndex)
    {
      if(!playedOnce)
      {
        currIndex = startIndex;
        playedOnce = true;
      }
      else
      {
        clearInterval(intv);
        currentAnimation = null;
        if(cbFunc != null)
          cbFunc();
      }
    }
  }, 0);//parseInt(1000.0 / (fps * animationSpeed)));
  currentAnimation = intv;
  
  imgObj.src = path + GetNumberString(currIndex, size) + '.png';
}

function SetStepInfo(title, body)
{
  var str = '<center>' + title + '</center>';
  document.getElementById('ui-title').innerHTML = str;
  str = '<center>' + body + '</center>';
  document.getElementById('ui-body').innerHTML = str;
}

function ShowStep(id)
{
  currentStep = id;
  SetStepInfo(steps[lid][id * 2], steps[lid][id * 2 + 1]);
  
  progressValue = 0;
  displayProgressValue = Math.min(90, id * 16);
}

function OnPlayerInteract(id)
{
  console.log(enablePlayer);
  if(!enablePlayer || isMiniVideoPlaying || !isFaceDetected) return;
  if(id == 0 && isPlayerOpenned) id = 1;
  
  var stepCallbacks = [TutorialStep2,TutorialStep3,TutorialStep4,TutorialStep5,TutorialStep6];
  
  switch(id)
  {
    case 0: // Open Player
      document.getElementById('ico-x').style.bottom = '1%';
      document.getElementById('ico-left').style.bottom = '1%';
      document.getElementById('ico-right').style.bottom = '1%';
      document.getElementById('ico-pause').style.bottom = '1%';
      document.getElementById('ico-play').style.bottom = '1%';
      document.getElementById('separator').style.bottom = '6%';
      
      document.getElementById('line').style.bottom = 'calc(11% + ' + (isiPad ? '6' : '2') + 'px)';
      document.getElementById('dot0').style.bottom = '11%';
      document.getElementById('dot1').style.bottom = '11%';
      document.getElementById('dot2').style.bottom = '11%';
      document.getElementById('dot3').style.bottom = '11%';
      document.getElementById('dot4').style.bottom = '11%';
      document.getElementById('dot5').style.bottom = '11%';
      
      document.getElementById('ui-bottombox').style.top = '65%';
      isPlayerOpenned = true;
      break;
    case 1: // Close Player
      document.getElementById('ico-x').style.bottom = '-50%';
      document.getElementById('ico-left').style.bottom = '-50%';
      document.getElementById('ico-right').style.bottom = '-50%';
      document.getElementById('ico-pause').style.bottom = '-50%';
      document.getElementById('ico-play').style.bottom = '-50%';
      document.getElementById('separator').style.bottom = '-50%';
      
      document.getElementById('line').style.bottom = 'calc(6% + ' + (isiPad ? '6' : '2') + 'px)';
      document.getElementById('dot0').style.bottom = '6%';
      document.getElementById('dot1').style.bottom = '6%';
      document.getElementById('dot2').style.bottom = '6%';
      document.getElementById('dot3').style.bottom = '6%';
      document.getElementById('dot4').style.bottom = '6%';
      document.getElementById('dot5').style.bottom = '6%';
      
      document.getElementById('ui-bottombox').style.top = '';
      isPlayerOpenned = false;
      break;
    case 2: // Prev Step
      if(currentStep == 0) return;
      if(progressValue < 0.2)
        currentStep = Math.max(0,currentStep - 1);
      
      if(currentAnimation != null)
        clearInterval(currentAnimation);
      if(currentFullScreenAnimation != null)
        clearInterval(currentFullScreenAnimation);
      document.getElementById('curtain').src = assetPath + imagePath + curtainEffectPath + '01.png';
      
      for(var i = 5; i > currentStep; --i)
        document.getElementById('dot' + i.toString()).style.backgroundColor = '#000000';
      
      ShowStep(currentStep);
      
      var progressBar = document.getElementById('line');
      progressBar.style.width = parseFloat(displayProgressValue + (progressValue * 16)) + '%';
      
      var start = (currentStep == 0) ? 0 : animImgFrames[currentStep-1];
      PlayAnimation(assetPath + imagePath + tutorialAnimationPath, start, animImgFrames[currentStep], 4, 20, isiPad ? animImgOffsetX[currentStep] : animImgOffsetX_phone, isiPad ? animImgOffsetY[currentStep] : animImgOffsetX_phone[currentStep], stepCallbacks[currentStep]);
      break;
    case 3: // Next Step
      if(currentStep == 4) return;
      currentStep = Math.min(4, currentStep + 1);
      if(currentAnimation != null)
        clearInterval(currentAnimation);
      if(currentFullScreenAnimation != null)
        clearInterval(currentFullScreenAnimation);
      document.getElementById('curtain').src = assetPath + imagePath + curtainEffectPath + '01.png';
      
      for(var i = 0; i <= currentStep; ++i)
        document.getElementById('dot' + i.toString()).style.backgroundColor = '#ffffff';
      
      ShowStep(currentStep);
      
      var progressBar = document.getElementById('line');
      progressBar.style.width = parseFloat(displayProgressValue + (progressValue * 16)) + '%';
      
      var start = animImgFrames[currentStep-1];
      PlayAnimation(assetPath + imagePath + tutorialAnimationPath, start, animImgFrames[currentStep], 4, 20, isiPad ? animImgOffsetX[currentStep]: animImgOffsetX_phone, isiPad ? animImgOffsetY[currentStep] : animImgOffsetX_phone[currentStep], stepCallbacks[currentStep]);
      break;
    case 4: // Pause Animation
      isPaused = true;
      break;
    case 5: // Player Animation
      isPaused = false;
      break;
  }
}

function TutorialStep6()
{
  PlayFullScreenAnimation(document.getElementById('curtain'), assetPath + imagePath + sparkleEffectPath, 0, 58, 2, 120, function()
  {
    gotoEndingScreen();
  });
  ShowStep(5);
  document.getElementById('dot5').style.backgroundColor = '#ffffff';
  document.getElementById('ui-player').style.display = 'none';
  updateHandler = null;
  isPaused = false;
  isDetectPaused = false;
}

function TutorialStep5()
{
  PlayFullScreenAnimation(document.getElementById('curtain'), assetPath + imagePath + curtainEffectPath, 1, 51, 2, 120, function()
  {
    PlayAnimation(assetPath + imagePath + tutorialAnimationPath, 400, 504, 4, 20, isiPad ? animImgOffsetX[currentStep] : animImgOffsetX_phone, isiPad ? animImgOffsetY[4] : animImgOffsetY_phone[4], TutorialStep6);
    ShowStep(4);
  });
  document.getElementById('dot4').style.backgroundColor = '#ffffff';
}

function TutorialStep4()
{
  PlayFullScreenAnimation(document.getElementById('curtain'), assetPath + imagePath + curtainEffectPath, 1, 51, 2, 120, function()
  {
    PlayAnimation(assetPath + imagePath + tutorialAnimationPath, 300, 400, 4, 20, isiPad ? animImgOffsetX[currentStep] : animImgOffsetX_phone, isiPad ? animImgOffsetY[3] : animImgOffsetY_phone[3], TutorialStep5);
    ShowStep(3);
  });
  document.getElementById('dot3').style.backgroundColor = '#ffffff';
}

function TutorialStep3()
{
  PlayFullScreenAnimation(document.getElementById('curtain'), assetPath + imagePath + curtainEffectPath, 1, 51, 2, 120, function()
  {
    PlayAnimation(assetPath + imagePath + tutorialAnimationPath, 200, 300, 4, 20, isiPad ? animImgOffsetX[currentStep] : animImgOffsetX_phone, isiPad ? animImgOffsetY[2] : animImgOffsetY_phone[2], TutorialStep4);
    ShowStep(2);
  });
  document.getElementById('dot2').style.backgroundColor = '#ffffff';
}

function TutorialStep2()
{
  PlayFullScreenAnimation(document.getElementById('curtain'), assetPath + imagePath + curtainEffectPath, 1, 51, 2, 120, function()
  {
    PlayAnimation(assetPath + imagePath + tutorialAnimationPath, 100, 200, 4, 20, isiPad ? animImgOffsetX[currentStep] : animImgOffsetX_phone, isiPad ? animImgOffsetY[1] : animImgOffsetY_phone[1], TutorialStep3);
    ShowStep(1);
  });
  document.getElementById('dot1').style.backgroundColor = '#ffffff';
}

function TutorialStep1()
{
  PlayAnimation(assetPath + imagePath + tutorialAnimationPath, 0, 100, 4, 20, isiPad ? animImgOffsetX[currentStep] : animImgOffsetX_phone, isiPad ? animImgOffsetY[0] : animImgOffsetY_phone[0], TutorialStep2);
  ShowStep(0);
}

function StartTutorial()
{
  isInTutorial = true;
  document.getElementById('ui-facedetect').style.pointerEvents = 'none';
  data_add_use_count();
  TutorialStep1();
}

var divAngleConst = 1.0 / 110.0;

function TutorialMode(face, brfv4)
{
  cameraXPreviousOffset = cameraXOffset;
  cameraXOffset += (cameraXTargetOffset - cameraXOffset) * 0.1;

  if(cameraXPreviousOffset != cameraXOffset)
    onResize(cameraXOffset);
  
  var mask = document.getElementById("mask");
  
  if(!isFaceDetected || face.state != brfv4.BRFState.FACE_TRACKING)
  {
    if(landmarkPositionHistory.length > 0)
    {
      landmarkPositionHistory.splice((nextLandmarkHistoryID+1)%landmarkPositionHistory.length,1);
    }
    if(landmarkPositionHistory.length == 0)
    {
      // Show cantDetectFaceAsset
       isDetectPaused = true;
      
      uiFaceCantDetect.style.opacity = cantDetectFaceOpacity.toString();
      mask.style.opacity = (1.0 - cantDetectFaceOpacity).toString();
      cantDetectFaceOpacity = Math.min(1.0,cantDetectFaceOpacity + 0.1);
      
      if(currentStep < 5)
      {
        document.getElementById('ui-player').style.display = 'none';
        document.getElementById('ui-bottombox').style.display = 'none';
        //mask.style.display = 'none';
      }
      
      if(autoReloadMode)
      {
        autoReloadDelayCounter += 0.02;
        if(autoReloadDelayCounter > autoReloadDelay)
        {
          updateHandler = null;
          layers[0].innerHTML = '';
          layers[1].innerHTML = '';
          layers[2].innerHTML = '';
          
          isFaceDetected = false;
          isDoneFadingOut = false;
          firstDetection = false;
          autoReloadDelayCounter = 0;
          progressValue = 0;
          displayProgressValue = 10;
          gotoWelcomeScreen();
        }
      }
      return;
    }
  }
  isDetectPaused = false;
  autoReloadDelayCounter = 0;
  uiFaceCantDetect.style.opacity = cantDetectFaceOpacity.toString();
  mask.style.opacity = (1.0 - cantDetectFaceOpacity).toString();
  cantDetectFaceOpacity = Math.max(0.0,cantDetectFaceOpacity - 0.1);
  if(currentStep < 5)
  {
    document.getElementById('ui-player').style.display = 'block';
    document.getElementById('ui-bottombox').style.display = 'flex';
    //mask.style.display = 'block';
  }
  
  var point = {x:0,y:0};
  if(isFaceDetected)
  {
    if(landmarkPositionHistory.length < landmarkHistoryCount)
      landmarkPositionHistory.push({x:face.points[30].x, y:face.points[30].y});
    else
    {
      landmarkPositionHistory[nextLandmarkHistoryID].x = face.points[30].x;
      landmarkPositionHistory[nextLandmarkHistoryID].y = face.points[30].y;
      nextLandmarkHistoryID = (nextLandmarkHistoryID + 1)%landmarkHistoryCount;
    }
    point.x = face.points[30].x;
    point.y = face.points[30].y;
  }
  else
  {
    var avgPoint = {x:landmarkPositionHistory[0].x, y:landmarkPositionHistory[0].y};
    for(var i = 1; i < landmarkPositionHistory.length; ++i)
    {
      avgPoint.x += landmarkPositionHistory[i].x;
      avgPoint.y += landmarkPositionHistory[i].y;
    }
    avgPoint.x /= landmarkPositionHistory.length;
    avgPoint.y /= landmarkPositionHistory.length;
    
    point = avgPoint;
  }
  
  var scaleX = face.scale / 480 * (1 - toDegree(Math.abs(face.rotationY)) * divAngleConst) * 2.5;
  var scaleY = face.scale / 480 * (1 - toDegree(Math.abs(face.rotationX)) * divAngleConst) * 2.5;
  
  var imageData = document.getElementById("_imageData");
  var maskimg = document.getElementById("maskimg");
  var x = point.x - maskimg.naturalWidth * scaleX * imgOffsetX;
  var y = point.y - maskimg.naturalHeight * scaleY * imgOffsetY;
  
  y = y * scaleCoeff + webcamOffsetY - toDegree(face.rotationZ) * divAngleConst * scaleCoeff * maskimg.naturalHeight * scaleY * imgOffsetY * 2.5;
  x = x * scaleCoeff + webcamOffsetX + toDegree(face.rotationZ) * divAngleConst * scaleCoeff * maskimg.naturalWidth * scaleX * imgOffsetX * 2.5;
  
  var xPercentage = face.points[30].x / imageData.width;
  cameraXTargetOffset = Math.max(minValue,Math.min(maxValue,(xPercentage - minValue) / diffValue));

  mask.style.transform = "matrix(" + scaleX + ",0,0," + scaleY + "," + x + "," + y + ") rotate(" + face.rotationZ + "rad)";
}

function gotoTutorialScreen()
{
  if(!isBRFInitialized) return;
  if(isTutorialMode || !isAssetsLoaded) return;
  //console.log(lid);
  isTutorialMode = true;
  videoBackground.innerHTML = '';
  FadeOut(function()
  {
    layers[2].innerHTML = '';
    
    ClearVideo();
    
    initTutorialScreen();
    FadeIn();
  });
}

function handleTrackingResults(
  brfv4,          // namespace
  faces,          // tracked faces
  imageDataCtx    // canvas context to draw into
)
{
  isFaceDetected = false;
  if(faces.length === 0) return;
  
  var face = faces[0];
  
  if(face.state === brfv4.BRFState.FACE_TRACKING_START
  || face.state === brfv4.BRFState.FACE_TRACKING) 
  {
    imageDataCtx.strokeStyle = "#00a0ff";
    isFaceDetected = true;
    
    if(debugMode)
    {
      for(var k = 0; k < face.vertices.length; k += 2) 
      {
        imageDataCtx.beginPath();
        imageDataCtx.arc(face.vertices[k], face.vertices[k + 1], 2, 0, 2 * Math.PI);
        imageDataCtx.stroke();
      }
    }
  }
  
  if(!isTutorialMode) return;
  
  if(updateHandler != null)
    updateHandler(face, brfv4);
};
// ------------------------------------------#
