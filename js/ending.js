// # -- ending functionality and operations -- #

var autoReloader = null;

function getCongratsScreen(path, lid)
{
  return path + endingPicPath[lid];
}

function gotoEndingScreen()
{
  data_add_complete_count();
  FadeOut(function()
  {
      layers[0].innerHTML = '';
      layers[1].innerHTML = '';
      layers[2].innerHTML = '';
      
      isFaceDetected = false;
      isDoneFadingOut = false;
      firstDetection = false;
      autoReloadDelayCounter = 0;
      progressValue = 0;
      displayProgressValue = 10;
      
      initEndingScreen();
      setTimeout(FadeIn, 1000);
  });
}

function initEndingScreen()
{
  var whiteScreen = '<div style="position:absolute; background-color: #ffffff; top:0;left:0;width:100%;height:100%; z-index:0;"></div>';
  
  videoBackground.innerHTML = whiteScreen;
  var endingImg = '<div style="position:absolute;top:0%;left:0%;width:100%;height:100%;" onclick="if(autoReloader != null) clearTimeout(autoReloader); gotoWelcomeScreen()">'
                + '<img src="' + getCongratsScreen(assetPath + imagePath, lid) +  '" style="height:100%;position:fixed; top:50%; left:50%; transform: translate(-50%,-50%);" />'
                + '</div>';
  var invButton = '<div id="hidden-button" style="top:0%;left:0%;width:100%;height:100%;" onclick="if(autoReloader != null) clearTimeout(autoReloader); gotoWelcomeScreen()"></div>';
  
  var body = invButton;
  
  layers[2].innerHTML = body;
  layers[2].innerHTML += endingImg;
  
  if(autoReloadMode)
  {
    autoReloader = setTimeout(function()
    {
      autoReloader = null;
      gotoWelcomeScreen();
    }, endScreenAutoReloadDelay * 1000);
  }
}
// ------------------------------------------#